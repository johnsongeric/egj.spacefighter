
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		// normal value 0.35, change to make it fire faster
		m_cooldownSeconds = 0.15;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		// check if its active, and and be used
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				// get the projectile
				Projectile *pProjectile = GetProjectile();
				if (pProjectile)
				{
					// Activate the projectile at the current position
					pProjectile->Activate(GetPosition(), true);
					// set the weapon's cooldown to the correct value
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};